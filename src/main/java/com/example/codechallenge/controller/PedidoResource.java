package com.example.codechallenge.controller;

import com.example.codechallenge.dao.PedidoRepository;
import com.example.codechallenge.model.Pedido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Optional;

@Component
public class PedidoResource {

    @Autowired
    private PedidoRepository pedidoRepository;

    public Iterable<Pedido> retrieveAllPedidos() {
        return pedidoRepository.findAll();
    }

    public Optional<Pedido> retrivePedido(Long id) {
        return pedidoRepository.findById(id);
    }

    public Iterable<Pedido> saveAll(Iterable<Pedido> listPedido) {
        return pedidoRepository.saveAll(listPedido);
    }

    public void deletePedido(Iterable<? extends Pedido> listPedido) {
        pedidoRepository.deleteAll(listPedido);
    }

    public BigDecimal sumTicketMedio(LocalDate before, LocalDate after) {
        return pedidoRepository.sumTicketMedio(before, after);
    }

    public Long countTicketMedio(LocalDate before, LocalDate after) {
        return pedidoRepository.countTicketMedio(before, after);
    }

    public BigDecimal ticketMedio(LocalDate before, LocalDate after) {
        BigDecimal resultado;
        resultado = sumTicketMedio(before, after).divide(BigDecimal.valueOf(countTicketMedio(before, after)), RoundingMode.CEILING);
        return resultado;
    }
}
