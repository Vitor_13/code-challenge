package com.example.codechallenge.controller;


import com.example.codechallenge.controller.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping("/financas")
public class FinancasController {

    @Autowired
    private PedidoResource pedidoResource;

    @GetMapping(path = "/{before}/to/{after}")
    public BigDecimal ticketMedio(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate before,
                                  @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate after) {
        return pedidoResource.ticketMedio(before, after);
    }
}
