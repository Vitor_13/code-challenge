package com.example.codechallenge.controller;

import com.example.codechallenge.dao.ItemRepository;
import com.example.codechallenge.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ItemResource {

    @Autowired
    private ItemRepository itemRepository;

    public Iterable<Item> retrieveAllItem(){
        return itemRepository.findAll();
    }

    public Optional<Item> retriveItem(Long id){
        return itemRepository.findById(id);
    }

    public Iterable<Item> saveAll(Iterable<Item> listItem) {
        return itemRepository.saveAll(listItem);
    }

    public void deleteItem(Iterable<? extends Item> listItem) {
        itemRepository.deleteAll(listItem);
    }

    public boolean existsById(Long id) {
        return itemRepository.existsById(id);
    }
}
