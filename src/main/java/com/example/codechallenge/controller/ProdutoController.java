package com.example.codechallenge.controller;

import com.example.codechallenge.controller.exception.NotFoundException;
import com.example.codechallenge.model.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoResource produtoResource;

    @GetMapping
    public Iterable<Produto> retrieveAllProdutos() {
        return produtoResource.retrieveAllProduto();
    }

    @GetMapping(path = "/{id}")
    public Optional<Produto> retriveProduto(@PathVariable Long id) {
        Optional<Produto> user = produtoResource.retriveProduto(id);

        if (!user.isPresent())
            throw new NotFoundException("codigo " + id + " nao encontrado!");

        return user;
    }

    @PostMapping(path = "/add")
    public ResponseEntity<Object> saveUser(@Valid @RequestBody Iterable<Produto> produto) {
        Iterable<Produto> savedProdutos = produtoResource.saveAll(produto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/produtos").buildAndExpand()
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(path = "/delete")
    public void deleteUser(@Valid @RequestBody Iterable<Produto> listRequestProduto) {
        List<Produto> produtoListDelete = new ArrayList<>();
        Iterator<Produto> produtoIterator = listRequestProduto.iterator();
               while (produtoIterator.hasNext()){
                   Produto produto = produtoIterator.next();
                   Optional<Produto> produtoRetrieve = retriveProduto(produto.getCodigo());
                   if (!produtoRetrieve.isPresent())
                       throw new NotFoundException("id " + produto.getCodigo() + " not found!");
                   produtoListDelete.add(produtoRetrieve.get());
               }
        produtoResource.deleteProduto(produtoListDelete);

    }

}
