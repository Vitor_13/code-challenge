package com.example.codechallenge.controller;

import com.example.codechallenge.controller.exception.NotFoundException;
import com.example.codechallenge.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemResource itemResource;

    @GetMapping
    public Iterable<Item> retrieveAllItens() {
        return itemResource.retrieveAllItem();
    }

    @GetMapping(path = "/{id}")
    public Optional<Item> retriveItem(@PathVariable Long id) {
        Optional<Item> user = itemResource.retriveItem(id);

        if (!user.isPresent())
            throw new NotFoundException("codigo " + id + " nao encontrado!");

        return user;
    }

    @PostMapping(path = "/add")
    @Transactional
    public ResponseEntity<Object> saveUser(@Valid @RequestBody Iterable<Item> item) {
        Iterable<Item> savedItens = itemResource.saveAll(item);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/").buildAndExpand()
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(path = "/delete")
    public void deleteUser(@Valid @RequestBody Iterable<Item> listRequestItem) {
        List<Item> itemListDelete = new ArrayList<>();
        Iterator<Item> itemIterator = listRequestItem.iterator();
        while (itemIterator.hasNext()){
            Item item = itemIterator.next();
            Optional<Item> produtoRetrieve = retriveItem(item.getId());
            if (!produtoRetrieve.isPresent())
                throw new NotFoundException("id " + item.getId() + " not found!");
            itemListDelete.add(produtoRetrieve.get());
        }
        itemResource.deleteItem(itemListDelete);

    }
}
