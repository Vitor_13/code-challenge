package com.example.codechallenge.controller;

import com.example.codechallenge.dao.ProdutoRepository;
import com.example.codechallenge.model.Produto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ProdutoResource {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> retrieveAllProduto() {
        return produtoRepository.findAll();
    }

    public Optional<Produto> retriveProduto(Long id) {
        return produtoRepository.findById(id);
    }

    public Iterable<Produto> saveAll(Iterable<Produto> listProduto) {
        return produtoRepository.saveAll(listProduto);
    }

    public void deleteProduto(Iterable<? extends Produto> listProduto) {
        produtoRepository.deleteAll(listProduto);
    }

    public boolean existsById(Long id) {
        return produtoRepository.existsById(id);
    }

}
