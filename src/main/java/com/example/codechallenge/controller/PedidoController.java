package com.example.codechallenge.controller;

import com.example.codechallenge.controller.PedidoResource;
import com.example.codechallenge.controller.exception.NotFoundException;
import com.example.codechallenge.model.Item;
import com.example.codechallenge.model.Pedido;
import com.example.codechallenge.model.Produto;
import com.fasterxml.jackson.databind.util.ArrayIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pedido")
public class PedidoController {

    @Autowired
    private PedidoResource pedidoResource;

    @Autowired
    private ItemResource itemResource;

    @GetMapping
    public Iterable<Pedido> retrieveAllPedidos() {
        return pedidoResource.retrieveAllPedidos();
    }

    @GetMapping(path = "/{id}")
    public Optional<Pedido> retrivePedido(@PathVariable Long id) {
        Optional<Pedido> pedido = pedidoResource.retrivePedido(id);

        if (!pedido.isPresent())
            throw new NotFoundException("codigo " + id + " nao encontrado!");

        return pedido;
    }

    @PostMapping(path = "/add")
    @Transactional
    public ResponseEntity<Object> savePedido(@Valid @RequestBody Iterable<Pedido> pedidos) {
        Boolean existItem = true;
        ArrayList<Long> idItem = new ArrayList<>();
        Iterator<Pedido> pedidosIterator = pedidos.iterator();
        while (pedidosIterator.hasNext()) {
            Iterator<Item> itemIterator = pedidosIterator.next().getItens().iterator();
            while (itemIterator.hasNext()) {
                Item itemJson = itemIterator.next();
                if (!itemResource.existsById(itemJson.getId())) {
                    existItem = false;
                    idItem.add(itemJson.getId());
                }
            }
        }

        if (existItem) {
            checarEstoque(pedidos);
            Iterable<Pedido> savedPedidos = pedidoResource.saveAll(pedidos);
            atualizarEstoque(savedPedidos);
        } else {
            throw new NotFoundException("Item(ns) nao encontrado! ID(s).: " + idItem.toString());
        }
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/").buildAndExpand()
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(path = "/delete")
    public void deletePedido(@Valid @RequestBody Iterable<Pedido> listRequestPedido) {
        List<Pedido> pedidoListDelete = new ArrayList<>();
        Iterator<Pedido> pedidoIterator = listRequestPedido.iterator();
        while (pedidoIterator.hasNext()) {
            Pedido pedido = pedidoIterator.next();
            Optional<Pedido> produtoRetrieve = retrivePedido(pedido.getCodigoIdentificador());
            if (!produtoRetrieve.isPresent())
                throw new NotFoundException("codigo " + pedido.getCodigoIdentificador() + " nao encontrado!");
            pedidoListDelete.add(produtoRetrieve.get());
        }
        pedidoResource.deletePedido(pedidoListDelete);

    }

    public void checarEstoque(Iterable<Pedido> pedidos) {
        Iterator<Pedido> pedidosIterator = pedidos.iterator();
        Boolean existEstoque = true;
        ArrayList<Long> idProduto = new ArrayList<>();
        while (pedidosIterator.hasNext()) {
            Iterator<Item> itemIterator = pedidosIterator.next().getItens().iterator();
            while (itemIterator.hasNext()) {
                Item itemJson = itemIterator.next();
                Produto produto = itemJson.getProduto();
                Long estoque = produto.getEstoque();
                if ((estoque - itemJson.getQuantidade()) < 0) {
                    existEstoque = false;
                    idProduto.add(itemJson.getProduto().getCodigo());
                }
            }
        }
        if (!existEstoque)
            throw new NotFoundException("Nao tem a quantidade do(s) produto(s) em estoque! Codigo(s).: " + idProduto.toString());
    }

    public void atualizarEstoque(Iterable<Pedido> pedidos) {
        Iterator<Pedido> pedidosIterator = pedidos.iterator();
        while (pedidosIterator.hasNext()) {
            Iterator<Item> itemIterator = pedidosIterator.next().getItens().iterator();
            while (itemIterator.hasNext()) {
                Item itemJson = itemIterator.next();
                Produto produto = itemJson.getProduto();
                Long estoque = produto.getEstoque();
                produto.setEstoque(estoque -= itemJson.getQuantidade());
            }
        }
    }
}
