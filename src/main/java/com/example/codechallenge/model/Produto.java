package com.example.codechallenge.model;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Detalhes de produtos")
@Entity
@Table(name = "produto")
public class Produto {

	@Id
	private Long codigo;
	private String nome;
	private String descricao;
	private Long estoque;
	private BigDecimal preco;
	private String atributos;

}
