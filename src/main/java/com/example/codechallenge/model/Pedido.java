package com.example.codechallenge.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
public class Pedido {

    @Id
    private Long codigoIdentificador;
    private LocalDate dataCompra;
    private String nomeComprador;
    private Estado estado;
    private BigDecimal valorFrete;
    @JsonProperty("itens")
    @JsonUnwrapped
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private List<Item> itens;

    public Pedido(Long codigoIdentificador, LocalDate dataCompra, String nomeComprador, Estado estado, BigDecimal valorFrete, List<Item> itens) {
        this.codigoIdentificador = codigoIdentificador;
        this.dataCompra = dataCompra;
        this.nomeComprador = nomeComprador;
        this.estado = estado;
        this.valorFrete = valorFrete;
        this.itens = itens;
    }

    public Pedido() {
    }
}
