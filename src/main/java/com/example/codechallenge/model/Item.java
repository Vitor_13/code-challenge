package com.example.codechallenge.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Detalhes de itens")
@Entity
@Table(name = "item")
public class Item {
	
	@Id
	@GeneratedValue
	private Long id;
    @JsonProperty("produto")
    @JsonDeserialize(as = Produto.class)
	@OneToOne(cascade = CascadeType.ALL,
			fetch = FetchType.EAGER)
	private Produto produto;
	private Long quantidade;
	private BigDecimal precoVenda;

}
