package com.example.codechallenge.dao;

import com.example.codechallenge.model.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ItemRepository extends CrudRepository<Item, Long> {

    @Override
    <S extends Item> S save(S s);

    @Override
    <S extends Item> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Optional<Item> findById(Long aLong);

    @Override
    boolean existsById(Long aLong);

    @Override
    Iterable<Item> findAll();

    @Override
    Iterable<Item> findAllById(Iterable<Long> iterable);

    @Override
    long count();

    @Override
    void deleteById(Long aLong);

    @Override
    void delete(Item item);

    @Override
    void deleteAll(Iterable<? extends Item> iterable);

    @Override
    void deleteAll();
}
