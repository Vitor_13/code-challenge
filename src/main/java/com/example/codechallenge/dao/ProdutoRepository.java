package com.example.codechallenge.dao;

import com.example.codechallenge.model.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {
    @Override
    <S extends Produto> S save(S s);

    @Override
    <S extends Produto> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Optional<Produto> findById(Long aLong);

    @Override
    boolean existsById(Long aLong);

    @Override
    Iterable<Produto> findAll();

    @Override
    Iterable<Produto> findAllById(Iterable<Long> iterable);

    @Override
    long count();

    @Override
    void deleteById(Long aLong);

    @Override
    void delete(Produto produto);

    @Override
    void deleteAll(Iterable<? extends Produto> iterable);

    @Override
    void deleteAll();

}
