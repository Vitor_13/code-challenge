package com.example.codechallenge.dao;

import com.example.codechallenge.model.Pedido;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

public interface PedidoRepository extends CrudRepository<Pedido, Long> {

    @Override
    <S extends Pedido> S save(S s);

    @Override
    <S extends Pedido> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Optional<Pedido> findById(Long aLong);

    @Override
    boolean existsById(Long aLong);

    @Override
    Iterable<Pedido> findAll();

    @Override
    Iterable<Pedido> findAllById(Iterable<Long> iterable);

    @Override
    long count();

    @Override
    void deleteById(Long aLong);

    @Override
    void delete(Pedido pedido);

    @Override
    void deleteAll(Iterable<? extends Pedido> iterable);

    @Override
    void deleteAll();

    @Query(value = "select sum(i.preco_venda) from item i\n" +
            "join pedido_itens pi on i.id = pi.itens\n" +
            "join pedido p on pi.pedido_codigo_identificador = p.codigo_identificador\n" +
            "where p.data_compra between :before and :after",nativeQuery = true)
    BigDecimal sumTicketMedio(@Param("before")LocalDate before, @Param("after")LocalDate after);

    @Query(value = "select count(codigo_identificador) from pedido p \n" +
            " where p.data_compra between :before and :after", nativeQuery = true)
    Long countTicketMedio(@Param("before")LocalDate before, @Param("after")LocalDate after);

}
